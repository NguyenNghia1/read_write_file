/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  Button,
  TextInput,
} from 'react-native';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tel: '',
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          placeholder="Enter text to save"
          onChangeText={(text) => this.setState({ tel: text })}
        />

        <TouchableOpacity style={styles.buttoncontainer} onPress={()=>this.writeToFile()}>
          <Text style={styles.buttontext}>Save File</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.buttoncontainer} onPress={()=>this.readFile()}>
          <Text style={styles.buttontext}>Read File</Text>
        </TouchableOpacity>

      </View>
    );
  }

  writeToFile() {
    if (this.state.tel.length == 0) {
      alert('Enter text, please!');
      return;
    }
    // require the module
    var RNFS = require('react-native-fs');

    // create a path you want to write to
    // :warning: on iOS, you cannot write into `RNFS.MainBundlePath`,
    // but `RNFS.DocumentDirectoryPath` exists on both platforms and is writable
    var path = 'file:///storage/emulated/0/documents/test.txt';

    // write the file
    RNFS.writeFile(path, this.state.tel, 'utf8')
      .then((success) => {
        alert('FILE WRITTEN!');
      })
      .catch((err) => {
        alert(err.message);
      });
  }

  readFile() {
    var RNFS = require('react-native-fs');
    var path = 'file:///storage/emulated/0/documents/test.txt';

    RNFS.readFile(path, 'utf8').then(res => {
      alert(res);
    })
      .catch(err => {
        console.log(err.message, err.code);
      });
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    paddingLeft: 20,
    paddingEnd: 20,
    borderRadius: 50,
    height: 50,
    fontSize: 25,
    backgroundColor: 'white',
    borderColor: '#1abc9c',
    borderWidth: 1,
    marginBottom: 20,
    color: '#34495e'
  },
  buttoncontainer: {
        height: 50,
        width: 200,
        borderRadius: 50,
        backgroundColor: '#1abc9c',
        paddingVertical: 10,
        justifyContent: 'center',
        marginBottom: 20
    },

    buttontext: {
        textAlign: 'center',
        color: '#ecf0f1',
        fontSize: 20
    }

});